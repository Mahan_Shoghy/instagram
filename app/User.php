<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Model\Comment;
use App\Model\Post;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'count_posts',
        'username',
        'img_profile',
        'bio',
        'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'username';
    }

    /**
     * Users posts
     *
     * @return HasMany
     */
    public function post()
    {
        return $this->hasMany(Post::class,"users_id","id")->latest();
    }

    /**
     * @return BelongsToMany
     */
    public function following() {
        return $this->belongsToMany(User::class, 'follow', 'users_id', 'following_users_id');
    }

    /**
     * @return BelongsToMany
     */
    public function follower() {
        return $this->belongsToMany(User::class, 'follow', 'following_users_id', 'users_id');
    }

    /**
     * @return HasMany
     */
    public function comment()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @param $id
     * @return void
     */
    public function follow($id)
    {
        return $this->following()->attach($id);
    }

    /**
     * @return int
     */
    public function unFollow()
    {
        return $this->follower()->detach(Auth()->id());
    }

    /**
     * @return MorphToMany
     */
    public function likePost()
    {
        return $this->morphedByMany(Post::class, "likeable","likeable");
    }

    /**
     * @return MorphToMany
     */
    public function likeComment()
    {
        return $this->morphedByMany(Comment::class,"likeable","likeable");
    }
}
