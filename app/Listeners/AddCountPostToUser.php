<?php

namespace App\Listeners;

use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddCountPostToUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $post
     * @return void
     */
    public function handle($post)
    {

        $user = User::query()->find(Auth()->id());
        $count_posts = $user->count_posts + 1;

        $user->update([
            "count_posts" => $count_posts
        ]);
    }
}
