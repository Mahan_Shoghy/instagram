<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddCountLikeToPost
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $post
     * @return void
     */
    public function handle($post)
    {
        $count_like = $post->post->count_like + 1;

        $post->post->update([
            "count_like" => $count_like
        ]);
    }
}
