<?php

namespace App\Listeners;

use App\Model\Media;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;

class DeleteMedia
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param $post
     * @return void
     */
    public function handle($post)
    {
        $post = $post->post;
        $files = Media::query()->where("post_id","=",$post->id)->get();

        foreach ($files as $file){
            Storage::disk("local")->delete("public/media/".$file->file);
        }
    }
}
