<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddCountLikeToComment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $comment
     * @return void
     */
    public function handle($comment)
    {
        $count_like = $comment->comment->count_like + 1;

        $comment->comment->update([
            "count_like" => $count_like
        ]);
    }
}
