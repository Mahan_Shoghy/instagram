<?php

namespace App\Http\Controllers;

use App\Events\DisLikeComment;
use App\Events\LikeComment;
use App\Model\Comment;
use Illuminate\Http\RedirectResponse;

class LikeCommentController extends Controller
{
    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function store(Comment $comment)
    {
        $comment->like();

        event(new LikeComment($comment));

        return back();
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment)
    {
        $comment->disLike();

        event(new DisLikeComment($comment));

        return back();
    }
}
