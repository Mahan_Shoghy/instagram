<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUser;
use App\Model\Media;
use App\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * @param User $user
     * @return Factory|View
     */
    public function show(User $user)
    {
        $post = $user->post()->paginate(10);

        $is_follow = false;

        if(AUth()->check()){
            // check auth user follow user or not and set a variable
            foreach ($user->follower as $follower){
                if ($follower->id === Auth()->id()){
                    $is_follow = true;
                }
            }

            return view('user', ["user" => $user, "posts" => $post , "is_follow" => $is_follow]);
        }

        return view('guestUesr', ["user" => $user, "posts" => $post , "is_follow" => $is_follow]);

    }

    /**
     * @return Factory|View
     */
    public function edit()
    {
        return view("editUser");
    }

    /**
     * @param UpdateUser $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UpdateUser $request, User $user)
    {

        $user->update([
            "name" => $request->name,
            "username" => $request->username,
            "email" => $request->email,
            "bio" => $request->bio,
        ]);

        return redirect()->route("user.show", [Auth()->user()->username]);
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route("home");
    }

}
