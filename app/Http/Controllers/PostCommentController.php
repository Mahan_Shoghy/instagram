<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Model\Comment;
use App\Model\Post;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PostCommentController extends Controller
{
    /**
     * @param CommentRequest $request
     * @param Post $post
     * @return RedirectResponse
     */
    public function store(CommentRequest $request,Post $post)
    {
        $post->addComment($request->content, $request->parent_id);

        return redirect()->route("post.show", [$post]);
    }

    /**
     * @param Comment $comment
     * @return Factory|View
     */
    public function edit(Comment $comment)
    {
        return view("editComment", ["comment" => $comment]);
    }

    /**
     * @param CommentRequest $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        $comment->update([
            "content" => $request->content,
        ]);

        return view("post", ["post" => $comment->post_id]);
    }

    /**
     * @param Comment $comment
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Comment $comment)
    {
        $comment->delete();

        return back();
    }

    /**
     * @param Comment $comment
     * @return Factory|View
     */
    public function reply(Comment $comment)
    {
        return view("reply", ["comment" => $comment]);
    }
}
