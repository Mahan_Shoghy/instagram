<?php

namespace App\Http\Controllers;

use App\Http\Classes\FollowingPosts;
use App\Model\Post;
use App\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class FollowController extends Controller
{
    use FollowingPosts;

    /**
     * @return Factory|View
     */
    public function index()
    {
        if (Auth()->check()){

            $posts = $this->getPosts();

            return view("posts",["posts" => $posts]);
        }

        $posts = Post::query()->orderByDesc("created_at")->paginate(10);

        return view("guest",["posts" => $posts]);
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    public function showFollowing(User $user)
    {
        $follows = $user->following;
        return view("list",["follows" => $follows]);
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    public function showFollower(User $user)
    {
        $follows = $user->follower;
        return view("list",["follows" => $follows]);
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function store(User $user)
    {
        Auth()->user()->follow($user->id);
        return back();
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user)
    {
        $user->unFollow();
        return back();
    }
}
