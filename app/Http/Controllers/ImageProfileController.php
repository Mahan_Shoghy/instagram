<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserProfile;
use App\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;

class ImageProfileController extends Controller
{
    /**
     * @param UserProfile $request
     * @param User $user
     * @return RedirectResponse
     */
    public function store(UserProfile $request, User $user)
    {
        $file = $request->file("img_profile");
        $stored_file = $file->store("public\profile");
        $file_name = pathinfo($stored_file)["basename"];

        $old_file = $user->img_profile;

        if ($old_file !== null) {
            Storage::disk("local")->delete(config("app.image_profile_path"). $old_file);
        }

        $user->update([
            "img_profile" => $file_name,
        ]);

        return redirect()->route("user.show", [Auth()->user()->username]);
    }

    /**
     * @param User $user
     * @return RedirectResponse
     */
    public function destroy(User $user)
    {
        $old_file = $user->img_profile;

        if ($old_file !== null) {
            Storage::disk("local")->delete(config("app.image_profile_path"). $old_file);

            $user->update([
                "img_profile" => null,
            ]);
        }

        return redirect()->route("user.show", [Auth()->user()->username]);
    }
}
