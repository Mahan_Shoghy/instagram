<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCreate;
use App\Http\Requests\PostUpdate;
use App\Model\Media;
use \App\Model\Post;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PostController extends Controller
{
    /**
     * @return Factory|View
     */
    public function create()
    {
        return view("addPost");
    }

    /**
     * @param PostCreate $request
     * @return RedirectResponse
     */
    public function store(PostCreate $request)
    {
        $caption = $request->caption;

        $post = Post::create([
            "caption" => $caption,
            "count_like" => 0,
            "count_comment" => 0,
            "users_id" => Auth()->id(),
        ]);

        $files = $request->file("media");

        foreach ($files as $file) {
            $stored_file = $file->store("media");

            $mime_file = pathinfo($stored_file)["extension"];

            $img_mime = [
                "jpeg",
                "png",
                "jpg"
            ];

            $mime_file = (in_array($mime_file, $img_mime)) ? "img" : "video";

            $file_name = pathinfo($stored_file)["basename"];
            $original_name = $file->getClientOriginalName();

            Media::create([
                "file" => $file_name,
                "original_name" => $original_name,
                "mime" => $mime_file,
                "post_id" => $post->id,
            ]);
        }

        // Update User PostCreated Event

        return redirect()->route("user.show", [Auth()->user()->username]);
    }

    /**
     * @param Post $post
     * @return RedirectResponse
     */
    public function show(Post $post)
    {
        return view("post", ["post" => $post]);
    }

    /**
     * @param Post $post
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(Post $post)
    {
        $post->delete();

        // Update User and Delete Media in PostDeleted Event

        return redirect()->route("user.show", [Auth()->user()->username]);
    }

    /**
     * @param Post $post
     * @return Factory|View
     */
    public function edit(Post $post)
    {
        return view("editPost", ["post" => $post]);
    }

    /**
     * @param PostUpdate $request
     * @param Post $post
     * @return RedirectResponse
     */
    public function update(PostUpdate $request ,Post $post)
    {
        $post->update([
            "caption" => $request->caption,
        ]);

        return redirect()->route("user.show", [Auth()->user()->username]);
    }
}
