<?php

namespace App\Http\Controllers;

use App\Model\Comment;
use App\Model\Post;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class LikeController extends Controller
{
    /**
     * @param Post $post
     * @return Factory|View
     */
    public function post(Post $post)
    {
        $user = $post->likeable;

        return view("list",["follows" => $user]);
    }

    /**
     * @param Comment $comment
     * @return Factory|View
     */
    public function comment(Comment $comment)
    {
        $user = $comment->likeable;

        return view("list",["follows" => $user]);
    }
}
