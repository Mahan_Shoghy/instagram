<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MediaDownloadController extends Controller
{
    public function download($file)
    {
        return Storage::download(config("app.media_path").$file);
    }
}
