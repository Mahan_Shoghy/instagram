<?php

namespace App\Http\Controllers;

use App\Events\DisLikePost;
use App\Events\LikePost;
use App\Listeners\AddCountLikeToPost;
use App\Listeners\RemoveCountLikeToPost;
use App\Model\Post;
use Illuminate\Http\RedirectResponse;

class LikePostController extends Controller
{
    /**
     * @param Post $post
     * @return string
     */
    public function store(Post $post)
    {
        $post->Like();

        event(new LikePost($post));

        return '<input width="30px;" type="image" onclick="like('.$post->id.')" id="like_img'.$post->id.'" src="'.asset("storage/like.png").'"
                value="'.route("post.dislike",[$post->id]).'">
                <a href="'.route("post.like.list",[$post->id]).'"
                style="margin-left: 9.5px;">'.$post->count_like.'</a>';
    }

    /**
     * @param Post $post
     * @return string
     */
    public function destroy(Post $post)
    {
        $post->disLike();

        event(new DisLikePost($post));

        return'<input width="30px;" type="image" onclick="like('.$post->id.')" id="like_img'.$post->id.'" src="'.asset("storage/unlike.png").'"
                value="'.route("post.like",[$post->id]).'">
                <a href="'.route("post.like.list",[$post->id]).'"
                style="margin-left: 9.5px;">'.$post->count_like.'</a>';
    }
}
