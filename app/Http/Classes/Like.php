<?php


namespace App\Http\Classes;


use App\User;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait Like
{
    /**
     * @return MorphToMany
     */
    public function likeable()
    {
        return $this->morphToMany(User::class, 'likeable',"likeable");
    }

    /**
     * method to like one post
     */
    public function like()
    {
        return $this->likeable()->attach(Auth()->id());
    }

    /**
     * method to dislike to post
     * @return int
     */
    public function disLike()
    {
        return $this->likeable()->detach(Auth()->id());
    }
}
