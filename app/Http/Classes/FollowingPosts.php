<?php


namespace App\Http\Classes;


use App\Model\Post;

trait FollowingPosts
{
    public function getPosts()
    {
        $followings = Auth()->user()->following()->pluck("id")->toArray();

        $followings[] = Auth()->id();

        return Post::query()->with(["media","users"])->whereIn("users_id",$followings)->orderByDesc("created_at")->paginate(10);
    }
}
