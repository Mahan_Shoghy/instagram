<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => ["required","max:16","string"],
            "username" => ["required","max:32","min:5",Rule::unique("users")->ignore(Auth()->id())],
            "email" => ["required","email:rfc",Rule::unique("users")->ignore(Auth()->id())],
            "bio" => ["nullable"],
        ];
    }
}
