<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "media" => ["required","array","max:5"],
            "media.*" => ["mimes:jpj,jpeg,png,mp4,mkv","max:".config("app.file_size")],
            "caption" => ['max:240'],
        ];
    }

    /**
     * show error message
     *
     * @return array
     */
    public function messages()
    {
        return [
            "media.*.max" => "The media may not be greater than ".(config('app.file_size')/1000)."mb."
        ];
    }
}
