<?php

namespace App\Providers;

use App\Events\DisLikeComment;
use App\Events\DisLikePost;
use App\Events\File;
use App\Events\LikeComment;
use App\Events\LikePost;
use App\Events\PostCreated;
use App\Events\PostDeleted;
use App\Listeners\AddCountLikeToComment;
use App\Listeners\AddCountLikeToPost;
use App\Listeners\DeleteMedia;
use App\Listeners\FileDelete;
use App\Listeners\FileStore;
use App\Listeners\RemoveCountLikeToComment;
use App\Listeners\RemoveCountLikeToPost;
use App\Listeners\RemoveCountPostToUser;
use App\Listeners\StoreMedia;
use App\Listeners\AddCountPostToUser;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        PostCreated::class => [
            AddCountPostToUser::class,
        ],
        PostDeleted::class => [
            RemoveCountPostToUser::class,
            DeleteMedia::class,
        ],
        LikePost::class => [
            AddCountLikeToPost::class,
        ],
        DisLikePost::class => [
            RemoveCountLikeToPost::class,
        ],
        LikeComment::class => [
            AddCountLikeToComment::class,
        ],
        DisLikeComment::class => [
            RemoveCountLikeToComment::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
