<?php

namespace App\Model;

use App\Http\Classes\Like;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Comment extends Model
{
    use Like;

    public $table = "comment";

    public $fillable = [
        'parent_id',
        'content',
        'users_id',
        'post_id',
        'count_like',
        'created_at',
        "updated_at",
    ];

    public $timestamps = true;

    public function users()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * @return HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class,"parent_id","id");
    }

    /**
     * @return BelongsTo
     */
    public function reply()
    {
        return $this->belongsTo(Comment::class);
    }

}
