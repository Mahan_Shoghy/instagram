<?php

namespace App\Model;

use App\Events\PostCreated;
use App\Events\PostDeleted;
use App\Http\Classes\Like;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

class Post extends Model
{
    use Like;

    public $table = "post";

    public $fillable = [
        "caption",
        "count_like",
        "created_at",
        "users_id"
    ];

    public $timestamps = true;

    public $dispatchesEvents = [
        "created" => PostCreated::class,
        "deleting" => PostDeleted::class,
    ];

    /**
     * @return BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function comment()
    {
        return $this->hasMany(Comment::class)->latest();
    }

    /**
     * @return HasMany
     */
    public function media()
    {
        return $this->hasMany(Media::class);
    }

    /**
     * @param $content
     * @param $parent_id
     * @return
     */
    public function addComment($content, $parent_id)
    {
        return Comment::create([
            "users_id" => Auth()->id(),
            "post_id" => $this->id,
            "parent_id" => $parent_id,
            "content" => $content,
        ]);
    }
}
