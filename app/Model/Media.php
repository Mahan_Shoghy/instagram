<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Media extends Model
{
    public $table = "media";

    public $fillable = [
        "original_name",
        "mime",
        "file",
        "post_id",
    ];

    public $timestamps = false;

    /**
     * @return BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
