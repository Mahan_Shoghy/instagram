<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


// Post Routes
Route::prefix("post")->group(function () {
    Route::get('/create', 'PostController@create')->middleware(["auth"])->name('post.create');
    Route::post('/create', 'PostController@store')->middleware(["auth"])->name('post.store');

    Route::get('/edit/{post}', 'PostController@edit')->middleware(["auth"])->name('post.edit');
    Route::patch('/{post}', 'PostController@update')->middleware(["auth"])->name('post.update');

    Route::get('/{post}', 'PostController@show')->name('post.show');

    Route::delete('/{post}', 'PostController@destroy')->middleware(["auth"])->name('post.delete');

    Route::get('/{post}/like', 'LikePostController@store')->middleware(["auth"])->name("post.like");
    Route::get('/{post}/dislike', 'LikePostController@destroy')->middleware(["auth"])->name("post.dislike");
});

// User Routes
Route::prefix("user")->group(function () {
    Route::get('/{user}', 'UserController@show')->name('user.show');

    Route::get('/edit/profile', 'UserController@edit')->middleware(["auth"])->name('user.edit');
    Route::patch('/{user}', 'UserController@update')->middleware(["auth"])->name('user.update');

    Route::post('/image/{user}', 'ImageProfileController@store')->middleware(["auth"])->name('user.store.image');
    Route::get('/image/{user}', 'ImageProfileController@destroy')->middleware(["auth"])->name('user.delete.image');

    Route::delete('/{user}', 'UserController@destroy')->middleware(["auth"])->name('user.delete');

});

// Home
Route::get('/', 'FollowController@index')->name('home');

// Download
Route::get('/download/{file}', 'MediaDownloadController@download')->name('download');

// Follow Routes
Route::prefix("/")->middleware(["auth"])->group(function () {
    Route::get("/{user}/following", "FollowController@showFollowing")->name("following.show");
    Route::get("/{user}/follower", "FollowController@showFollower")->name("follower.show");

    Route::post('/{user}', "FollowController@store")->name("follow.store");
    Route::delete('/{user}', "FollowController@destroy")->name("follow.delete");
});

// CommentRequest Route
Route::prefix("comment")->middleware(["auth"])->group(function () {
    Route::post('/{post}', 'PostCommentController@store')->name("comment.store");
    Route::get('/{comment}/reply', 'PostCommentController@reply')->name("comment.reply");

    Route::delete('/{comment}', 'PostCommentController@destroy')->name("comment.delete");

    Route::get('/{comment}', 'PostCommentController@edit')->name("comment.edit");
    Route::patch('/{comment}', 'PostCommentController@update')->name("comment.update");

    Route::get('/{comment}/like', 'LikeCommentController@store')->name("comment.like");
    Route::get('/{comment}/dislike', 'LikeCommentController@destroy')->name("comment.dislike");
});

// List Like Route
Route::prefix("like")->group(function (){
   Route::get('/list/{post}', 'LikeController@post')->name('post.like.list');

   Route::get('/list/{comment}', 'LikeController@comment')->name('comment.like.list');
});



