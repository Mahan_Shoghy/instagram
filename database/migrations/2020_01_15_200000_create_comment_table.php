<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'comment';

    /**
     * Run the migrations.
     * @table comment
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->text('content');
            $table->integer('count_like')->default(0);
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('post_id');
            $table->timestamps();

            $table->index(["post_id"], 'fk_comment_post_idx');

            $table->index(["users_id"], 'fk_comment_users_idx');

            $table->index(["parent_id"], 'fk_comment_comment_idx_idx');


            $table->foreign('post_id', 'fk_comment_post_idx')
                ->references('id')->on('post')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('parent_id', 'fk_comment_comment_idx_idx')
                ->references('id')->on('comment')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('users_id', 'fk_comment_users_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
