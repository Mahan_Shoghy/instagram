<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'follow';

    /**
     * Run the migrations.
     * @table follow
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('users_id');
            $table->unsignedInteger('following_users_id');

            $table->unique(['users_id', 'following_users_id']);

            $table->index(["following_users_id"], 'fk_users_has_users_users1_idx');

            $table->index(["users_id"], 'fk_users_has_users_users2_idx');


            $table->foreign('following_users_id', 'fk_users_has_users_users1_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('users_id', 'fk_users_has_users_users2_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
