<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'post';

    /**
     * Run the migrations.
     * @table post
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('caption')->nullable();
            $table->integer('count_like');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
            $table->unsignedInteger('users_id');

            $table->index(["users_id"], 'fk_post_users_idx');


            $table->foreign('users_id', 'fk_post_users_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
