<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'media';

    /**
     * Run the migrations.
     * @table media
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('original_name', 191);
            $table->string('file', 100);
            $table->string('mime', 45);
            $table->unsignedInteger('post_id');

            $table->index(["post_id"], 'fk_media_post1_idx');


            $table->foreign('post_id', 'fk_media_post1_idx')
                ->references('id')->on('post')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
