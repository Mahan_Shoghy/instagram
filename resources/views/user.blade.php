@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">{{ $user->username }}</div>

                    @if (session('status'))
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-3 ml-3 mt-3 mb-3">
                            <img style="border:black 0.3px solid; border-radius: 100%;"
                                 src='@if($user->img_profile == null)
                                 {{ asset("storage/profile/default.png") }}
                                 @else
                                 {{ asset("storage/profile/".$user->img_profile) }}
                                 @endif
                                     ' alt="profile" width="150px" height="150px">
                        </div>

                        <div class="col-md-8">
                            <div class="row mt-4">
                                <div class="col-md-6" style="height:100px;">
                                    <div style="opacity: 75%;">bio:</div>
                                    <p style="font-size:12px;">{{ $user->bio }}</p>
                                </div>
                                    @if(Auth()->user()->username === $user->username)
                                        <div class="row col-md-6 mt-2">
                                            <div class="col-md-4 mt-3">
                                                <a href="{{ route("follower.show",[Auth()->user()->username]) }}">
                                                    <button class="btn btn-success">
                                                        <div style="padding:5px; margin-bottom:5px;">Follower</div>
                                                        {{ $user->follower->count() }}
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="col-md-4 mt-3">
                                                <a href="{{ route("following.show",[Auth()->user()->username]) }}">
                                                    <button class="btn btn-success">
                                                        <div style="padding:5px; margin-bottom:5px;">Following</div>
                                                        {{ $user->following->count() }}
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="col-md-4 mt-3">
                                                <button class="btn btn-success">
                                                    <div style="padding:5px; margin-bottom:5px;">Posts</div>
                                                    {{ $user->count_posts }}
                                                </button>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row col-md-6 mt-2">
                                            <div class="col-md-4 mt-3">
                                                @if($is_follow)
                                                    <form action="{{ route("follow.delete",[$user->username]) }}"
                                                          method="post">
                                                        @csrf
                                                        @method("delete")
                                                        <button type="submit" class="btn btn-danger">
                                                            <div style="padding:5px;">Unfollow</div>
                                                        </button>
                                                        <a href="{{ route("follower.show",[$user->username]) }}">
                                                            <div class="btn btn-danger">
                                                                {{ $user->follower->count() }}
                                                            </div>
                                                        </a>
                                                    </form>
                                                @else
                                                    <form action="{{ route("follow.store",[$user->username]) }}"
                                                          method="post">
                                                        @csrf
                                                        <button type="submit" class="btn btn-success">
                                                            <div style="padding:5px;">Follow</div>
                                                            {{ $user->follower->count() }}
                                                        </button>
                                                    </form>
                                                @endif
                                            </div>
                                            <div class="col-md-4 mt-3">
                                                <a href="{{ route("following.show",[$user->username]) }}">
                                                    <button class="btn btn-success">
                                                        <div style="padding:5px; margin-bottom:5px;">Following</div>
                                                        {{ $user->following->count() }}
                                                    </button>
                                                </a>
                                            </div>
                                            <div class="col-md-4 mt-3">
                                                <button class="btn btn-success">
                                                    <div style="padding:5px; margin-bottom:5px;">Posts</div>
                                                    {{ $user->count_posts }}
                                                </button>
                                            </div>
                                        </div>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-9">
                <div class="row">
                    <div class="w-100 justify-content-center">{{ $posts->links() }}</div>
                    @foreach($posts as $post)
                        <div class="col-md-6 mt-3">
                            <div class="card" style="height: 400px; overflow-y: auto; overflow-x: hidden;">

                                    @if(Auth()->user()->username == $user->username)
                                        <div class="btn-group dropdown">
                                            <img src="{{ asset("storage/3dot.png") }}" class="m-1 mt-2"
                                                 data-toggle="dropdown"
                                                 aria-haspopup="true"
                                                 aria-expanded="false">
                                            <div class="dropdown-menu dropdown-menu-right"
                                                 aria-labelledby="navbarDropdown">
                                                <div class="dropdown-item">
                                                    <a href="{{ route("post.edit",[$post->id]) }}">
                                                        <button type="submit" class="btn btn-light">
                                                            Edit
                                                        </button>
                                                    </a>
                                                </div>
                                                <div class="dropdown-item">
                                                    <form method="post" action="{{ route("post.delete",[$post->id]) }}">
                                                        @csrf
                                                        @method("delete")
                                                        <button type="submit" class="btn btn-light">
                                                            Delete
                                                        </button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endif

                                <div id="carouselExampleInterval{{ $post->id }}" class="carousel slide"
                                     data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php
                                        $num = 1;
                                        ?>
                                        <a href="{{ route("post.show",[$post->id])}}">
                                            @foreach($post->media as $media)
                                                <div class="carousel-item @if($num === 1) active @endif">
                                                    @if($media->mime == "img")
                                                        <img src="{{ route('download',[$media->file]) }}"
                                                             style="height: 200px;" class="w-100">
                                                    @else
                                                        <video class="w-100" style="height: 200px;" controls>
                                                            <source src="{{ route('download',[$media->file]) }}">
                                                            Your browser does not support HTML5 video.
                                                        </video>
                                                    @endif
                                                </div>
                                                <?php
                                                $num += 1;
                                                ?>
                                            @endforeach
                                        </a>
                                    </div>
                                    @if($post->media->count() > 1)
                                        <a class="carousel-control-prev"
                                           href="#carouselExampleInterval{{ $post->id }}"
                                           role="button"
                                           data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next"
                                           href="#carouselExampleInterval{{ $post->id }}"
                                           role="button"
                                           data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    @endif
                                </div>
                                <div class="row mt-2" style="margin-left: 0.1%;">
                                    <div id="like_div{{ $post->id }}" class="col-md-1">
                                        @if(!$post->likeable->contains('id', Auth()->id()))
                                            <input width="30px;" type="image" onclick="like({{ $post->id }})" id="like_img{{ $post->id }}" src="{{ asset("storage/unlike.png") }}"
                                                   value="{{ route("post.like",[$post->id]) }}">
                                            <a href="{{ route("post.like.list",[$post->id]) }}"
                                               style="margin-left: 9.5px;">{{ $post->count_like }}</a>
                                        @else
                                            <input width="30px;" type="image" onclick="like({{ $post->id }})" id="like_img{{ $post->id }}" src="{{ asset("storage/like.png") }}"
                                                   value="{{ route("post.dislike",[$post->id]) }}">
                                            <a href="{{ route("post.like.list",[$post->id]) }}"
                                               style="margin-left: 9.5px;">{{ $post->count_like }}</a>
                                        @endif
                                    </div>
                                    <div class="col-md-1">
                                        <a href="{{ route("post.show", [$post->id]) }}">
                                            <img src="{{ asset("storage/comment.png") }}">
                                            <div style="margin-left: 9.5px;">{{ $post->comment->count() }}</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <h6 class="card-subtitle mb-2 text-muted">{{ $post->created_at->diffForHumans() }}</h6>
                                    <p class="card-text">{{ $post->caption }}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
