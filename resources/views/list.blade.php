@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($follows as $follow)
                    <div class="card-header">
                        <div>
                            <a href="{{ route("user.show", [$follow]) }}">
                                <img style="border-radius: 100%; border: black 0.5px solid" src="
                                    @if($follow->img_profile == null)
                                {{ asset("storage/profile/default.png") }}
                                @else
                                {{ asset("storage/profile/".$follow->img_profile) }}
                                @endif " alt="add post" width="35px" height="35px">
                                ---- {{ $follow->username }}
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    </div>
@endsection
