<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Instagram')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/jquery-3.4.1.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style rel="stylesheet">
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            height: 85px;
        }
    </style>
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset("storage\instagram-apk.jpg") }}" alt=""> | @yield('title', 'Instagram')
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth()->user()->username }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <div class="dropdown-item">
                                    <a href="{{ route('user.edit') }}">
                                        Edit
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                </div>
                                <div class="dropdown-item">
                                    <form method="post" action="{{ route('user.delete',[Auth()->user()->username]) }}">
                                        @csrf
                                        @method("delete")
                                        <button class="btn btn-danger" type="submit">
                                            Delete Account
                                        </button>
                                    </form>

                                </div>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
    <main class="py-4">
        @yield('content')
    </main>
    @if (\Illuminate\Support\Facades\Auth::check())
        <footer class="footer modal-footer bg-white shadow-sm w-100" style="height: 50px;">
            <div class="container justify-content-center">
                <div class="row">
                    <div class="col-md-4">
                        <a style="margin-left:45%" href="{{ route("home") }}">
                            <img src="{{ asset('storage\69524.png') }}" alt="home" width="35px" height="35px">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a style="margin-left:45%" href="{{ route("post.create") }}">
                            <img src="{{ asset('storage\add-512.png') }}" alt="add post" width="35px" height="35px">
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a style="margin-left:45%" href="{{ route('user.show',[Auth()->user()->username]) }}">
                            <img style="border:black 0.3px solid; border-radius: 100%;" src="
                            @if(Auth()->user()->img_profile == null)
                                {{ asset("storage/profile/default.png") }}
                            @else
                                {{ asset("storage/profile/".Auth()->user()->img_profile) }}
                            @endif " alt="profile" width="35px" height="35px">
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    @endif
</div>
</body>
<script>
    function like(id) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("like_div"+id).innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", document.getElementById("like_img"+id).value , true);
        xhttp.send();
    }
</script>
</html>
