@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Post</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <div class="input-group row">
                                    <label for="media"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Select Files') }}</label>
                                    <div class="custom-file">
                                        <input id="media" class="@error('media') is-invalid @enderror" type="file"
                                               name="media[]" multiple>
                                    </div>
                                </div>
                                @foreach($errors->all() as $message)
                                    <div class="alert alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </div>
                                @endforeach
                            </div>

                            <div class="form-group row">
                                <label for="caption"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Caption') }}</label>

                                <div class="col-md-6">
                                    <textarea id="caption" type="text"
                                              class="form-control @error('caption') is-invalid @enderror" name="caption"
                                              autocomplete="caption" rows="8"></textarea>

                                    @error('caption')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Add Post
                                    </button>
                                </div>
                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
