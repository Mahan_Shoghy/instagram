@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Comment</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('comment.update',[$comment->id]) }}">
                            @csrf
                            @method("patch")

                            <div class="form-group row">
                                <label for="content"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Content') }}</label>

                                <div class="col-md-6">
                                    <textarea id="content" type="text"
                                              class="form-control @error('content') is-invalid @enderror" name="content"
                                              autocomplete="content" rows="8">{{ $comment->content }}</textarea>

                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="reset" class="btn btn-primary">
                                        Reset
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Edit Comment
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
