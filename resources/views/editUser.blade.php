@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route("user.store.image",[Auth()->user()->username]) }}"
                              enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <div class="input-group row">
                                    <label for="img_profile"
                                           class="col-md-4 col-form-label text-md-right">{{ __('Image Profile') }}</label>
                                    <div class="custom-file">
                                        <input name="img_profile" type="file"
                                               class="custom-file-input @error('img_profile') is-invalid @enderror"
                                               id="img_profile"
                                               aria-describedby="inputGroupFileAddon01">
                                        <label class="custom-file-label" for="img_profile">Choose </label>
                                    </div>

                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Set New Image
                                        </button>
                                        <a href="{{ route("user.delete.image",[Auth()->user()->username]) }}">
                                            <input value="Delete Own Image" type="button" class="btn btn-danger">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @error('img_profile')
                        <span class="alert-danger" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                        @enderror
                        <form method="POST" action="{{ route('user.update',[Auth()->user()->username]) }}">
                            @csrf
                            @method("patch")
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ Auth()->user()->name }}" required autocomplete="name" autofocus>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="username"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Username') }}</label>

                                <div class="col-md-6">
                                    <input id="username" type="text"
                                           class="form-control @error('username') is-invalid @enderror" name="username"
                                           value="{{ Auth()->user()->username }}" required autocomplete="username" autofocus>

                                    @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror" name="email"
                                           value="{{ Auth()->user()->email }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="bio"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Bio') }}</label>

                                <div class="col-md-6">
                                    <textarea id="bio" type="text"
                                              class="form-control @error('bio') is-invalid @enderror" name="bio"
                                              autocomplete="bio" rows="8">{{ Auth()->user()->bio }}</textarea>

                                    @error('bio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            @error('img_profile')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror

                            <br>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Edit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
