@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Post</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('post.update',[$post->id]) }}">
                            @csrf
                            @method("patch")

                            <div class="form-group row">
                                <label for="caption"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Caption') }}</label>

                                <div class="col-md-6">
                                    <textarea id="caption" type="text"
                                              class="form-control @error('caption') is-invalid @enderror" name="caption"
                                              autocomplete="caption" rows="8">{{ $post->caption }}</textarea>

                                    @error('caption')
                                    <span class="alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="reset" class="btn btn-primary">
                                        Reset
                                    </button>
                                    <button type="submit" class="btn btn-primary">
                                        Edit Post
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
