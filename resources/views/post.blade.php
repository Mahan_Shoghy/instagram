@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if(Auth()->id() === $post->users_id)
                        <div class="card-header bg-white">
                            <div class="btn-group dropdown">
                                <img src="{{ asset("storage/3dot.png") }}" class="m-1 mt-2"
                                     data-toggle="dropdown"
                                     aria-haspopup="true"
                                     aria-expanded="false">
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <div class="dropdown-item">
                                        <a href="{{ route("post.edit",[$post->id]) }}">
                                            <button type="submit" class="btn btn-light">
                                                Edit
                                            </button>
                                        </a>
                                    </div>
                                    <div class="dropdown-item">
                                        <form method="post" action="{{ route("post.delete",[$post->id]) }}">
                                            @csrf
                                            @method("delete")
                                            <button type="submit" class="btn btn-light">
                                                Delete
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="card-header">
                            <div>
                                <a href="{{ route("user.show", [$post->users->username]) }}">
                                    <img style="border-radius: 100%; border: black 0.5px solid" src="
                                    @if($post->users->img_profile == null)
                                    {{ asset("storage/profile/default.png") }}
                                    @else
                                    {{ asset("storage/profile/".$post->users->img_profile) }}
                                    @endif " alt="add post" width="35px" height="35px">
                                    ---- {{ $post->users->username }}

                                </a>
                                <div style="display: inline-block; float: right"
                                     class="card-subtitle text-muted">{{ $post->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-12 mt-3">
                        <div id="carouselExampleInterval{{ $post->id }}" class="carousel slide"
                             data-ride="carousel">
                            <div class="carousel-inner">
                                <?php
                                $num = 1;
                                ?>
                                @foreach($post->media as $media)
                                    <div class="carousel-item @if($num === 1) active @endif">
                                        @if($media->mime == "img")
                                            <img src="{{ route('download',[$media->file]) }}"
                                                 height="400px" class="w-100">
                                        @else
                                            <video class="w-100" style="height: 400px;" controls>
                                                <source src="{{ route('download',[$media->file]) }}">
                                                Your browser does not support HTML5 video.
                                            </video>
                                        @endif
                                    </div>
                                    <?php
                                    $num += 1;
                                    ?>
                                @endforeach
                            </div>
                            @if($post->media->count() > 1)
                                <a class="carousel-control-prev"
                                   href="#carouselExampleInterval{{ $post->id }}"
                                   role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next"
                                   href="#carouselExampleInterval{{ $post->id }}"
                                   role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            @endif
                        </div>
                        <div class="row mt-2" style="margin-left: 0.1%;">
                            <div id="like_div{{ $post->id }}" class="col-md-1">
                                @if(!$post->likeable->contains('id', Auth()->id()))
                                    <input width="30px;" type="image" onclick="like({{ $post->id }})" id="like_img{{ $post->id }}" src="{{ asset("storage/unlike.png") }}"
                                           value="{{ route("post.like",[$post->id]) }}">
                                    <a href="{{ route("post.like.list",[$post->id]) }}"
                                       style="margin-left: 9.5px;">{{ $post->count_like }}</a>
                                @else
                                    <input width="30px;" type="image" onclick="like({{ $post->id }})" id="like_img{{ $post->id }}" src="{{ asset("storage/like.png") }}"
                                           value="{{ route("post.dislike",[$post->id]) }}">
                                    <a href="{{ route("post.like.list",[$post->id]) }}"
                                       style="margin-left: 9.5px;">{{ $post->count_like }}</a>
                                @endif
                            </div>
                        </div>
                        <div class="card-body">
                            <h6 class="card-subtitle mb-2 text-muted">{{ $post->created_at->diffForHumans() }}</h6>
                            <p class="card-text">{{ $post->caption }}</p>
                        </div>
                        <div style="margin-top: -30px;" class="card-body">
                            <form action="{{ route("comment.store", [$post->id]) }}" method="post">
                                @csrf
                                <input placeholder="Add Comment" name="content" class="form-group" type="text">
                                <button class="btn btn-info">Add Comment</button>
                            </form>
                            @error("content")
                                <span class="alert-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div>
                                <b>Comments:</b>
                            </div>
                            @foreach($post->comment as $comment)
                                @if(!$comment->parent_id)
                                    <div class="col-md-12 row">
                                        <div class="col-md-3">
                                            <a href="{{ route("comment.like.list",[$comment->id]) }}"
                                               style="font-size: 10px;">{{ $comment->count_like }}</a>
                                            @if(!$comment->likeable->contains('id', Auth()->id()))
                                                <a href="{{ route("comment.like",[$comment->id]) }}"><img
                                                        src="{{ asset("storage/unlike.png") }}"></a>
                                            @else
                                                <a href="{{ route("comment.dislike",[$comment->id]) }}"><img
                                                        src="{{ asset("storage/like.png") }}"></a>
                                            @endif
                                            <a href="{{ route("user.show", [$comment->users->username]) }}">
                                                <img style="border-radius: 100%; border: black 0.5px solid" src="
                                            @if($comment->users->img_profile == null)
                                                {{ asset("storage/profile/default.png") }}
                                                @else
                                                {{ asset("storage/profile/".$comment->users->img_profile) }}
                                                @endif " alt="add post" width="35px" height="35px">
                                                {{ $comment->users->username }}
                                            </a>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="w-100"
                                                 style="text-align:left; display: inline-block;">{{ $comment->content }}</div>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="{{ route("comment.reply",[$comment->id]) }}">Reply</a>
                                            <div style="display: inline-block;"
                                                 class="card-subtitle text-muted">{{ $comment->created_at->diffForHumans() }}</div>
                                        </div>
                                        <div class="col-md-3">
                                            <button class="btn btn-light" type="button" data-toggle="collapse"
                                                    data-target="#collapse{{ $comment->id }}" aria-expanded="false"
                                                    aria-controls="collapse{{ $comment->id }}">
                                                Replys
                                            </button>
                                            @foreach($post->comment as $reply)
                                                @if($reply->parent_id === $comment->id)
                                                    <div class="collapse"
                                                         style="width: 600px; background-color: white; padding: 20px; border:black 2px solid; border-radius: 5px;"
                                                         id="collapse{{ $comment->id }}">
                                                        <div class="col-md-12 row">
                                                            <div class="col-md-3">
                                                                <a href="{{ route("comment.like.list",[$reply->id]) }}"
                                                                   style="font-size: 10px;">{{ $reply->count_like }}</a>
                                                                @if(!$reply->likeable->contains('id', Auth()->id()))
                                                                    <a href="{{ route("comment.like",[$reply->id]) }}"><img
                                                                            src="{{ asset("storage/unlike.png") }}"></a>
                                                                @else
                                                                    <a href="{{ route("comment.dislike",[$reply->id]) }}"><img
                                                                            src="{{ asset("storage/like.png") }}"></a>
                                                                @endif
                                                                <a href="{{ route("user.show", [$reply->users->username]) }}">
                                                                    <img
                                                                        style="border-radius: 100%; border: black 0.5px solid"
                                                                        src="
                                            @if($reply->users->img_profile == null)
                                                                        {{ asset("storage/profile/default.png") }}
                                                                        @else
                                                                        {{ asset("storage/profile/".$reply->users->img_profile) }}
                                                                        @endif " alt="add post" width="35px"
                                                                        height="35px">
                                                                    {{ $reply->users->username }}
                                                                </a>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="w-100"
                                                                     style="text-align:left; display: inline-block;">{{ $reply->content }}</div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <a href="{{ route("comment.reply",[$reply->id]) }}">Reply</a>
                                                                <div style="display: inline-block;"
                                                                     class="card-subtitle text-muted">{{ $reply->created_at->diffForHumans() }}</div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <button class="btn btn-light" type="button"
                                                                        data-toggle="collapse"
                                                                        data-target="#collapse{{ $reply->id }}"
                                                                        aria-expanded="false"
                                                                        aria-controls="collapse{{ $reply->id }}">
                                                                    Replys
                                                                </button>
                                                                @foreach($post->comment as $reply_to_reply)
                                                                    @if($reply_to_reply->parent_id === $reply->id)
                                                                        <div class="collapse"
                                                                             style="width: 600px; background-color: white;  padding: 20px; border:black 2px solid; border-radius: 5px;"
                                                                             id="collapse{{ $reply->id }}">
                                                                            <div class="col-md-12 row">
                                                                                <div class="col-md-3">
                                                                                    <a href="{{ route("comment.like.list",[$reply_to_reply->id]) }}"
                                                                                       style="font-size: 10px;">{{ $reply_to_reply->count_like }}</a>
                                                                                    @if(!$reply_to_reply->likeable->contains('id', Auth()->id()))
                                                                                        <a href="{{ route("comment.like",[$reply_to_reply->id]) }}"><img
                                                                                                src="{{ asset("storage/unlike.png") }}"></a>
                                                                                    @else
                                                                                        <a href="{{ route("comment.dislike",[$reply_to_reply->id]) }}"><img
                                                                                                src="{{ asset("storage/like.png") }}"></a>
                                                                                    @endif
                                                                                    <a href="{{ route("user.show", [$reply_to_reply->users->username]) }}">
                                                                                        <img
                                                                                            style="border-radius: 100%; border: black 0.5px solid"
                                                                                            src="
                                            @if($reply->users->img_profile == null)
                                                                                            {{ asset("storage/profile/default.png") }}
                                                                                            @else
                                                                                            {{ asset("storage/profile/".$reply_to_reply->users->img_profile) }}
                                                                                            @endif " alt="add post"
                                                                                            width="35px" height="35px">
                                                                                        {{ $reply_to_reply->users->username }}
                                                                                    </a>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <div class="w-100"
                                                                                         style="text-align:left; display: inline-block;">{{ $reply_to_reply->content }}</div>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <a href="{{ route("comment.reply",[$reply_to_reply->id]) }}">Reply</a>
                                                                                    <div style="display: inline-block;"
                                                                                         class="card-subtitle text-muted">{{ $reply_to_reply->created_at->diffForHumans() }}</div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    @if(Auth()->id() === $reply_to_reply->users->id)
                                                                                        <form method="post"
                                                                                              action="{{ route("comment.delete",[$reply_to_reply->id]) }}">
                                                                                            @csrf
                                                                                            @method("delete")
                                                                                            <input width="20px"
                                                                                                   type="image"
                                                                                                   src="{{ asset("storage/x.png") }}"
                                                                                                   alt="Delete">
                                                                                            <a class="ml-3"
                                                                                               href="{{ route("comment.edit",[$reply_to_reply->id]) }}">Edit</a>
                                                                                        </form>
                                                                                    @endif
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endforeach
                                                                @if(Auth()->id() === $reply->users->id)
                                                                    <form method="post"
                                                                          action="{{ route("comment.delete",[$reply->id]) }}">
                                                                        @csrf
                                                                        @method("delete")
                                                                        <input width="20px" type="image"
                                                                               src="{{ asset("storage/x.png") }}"
                                                                               alt="Delete">
                                                                        <a class="ml-3"
                                                                           href="{{ route("comment.edit",[$reply->id]) }}">Edit</a>
                                                                    </form>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                            @if(Auth()->id() === $comment->users->id)
                                                <form method="post"
                                                      action="{{ route("comment.delete",[$comment->id]) }}">
                                                    @csrf
                                                    @method("delete")
                                                    <input width="20px" type="image" src="{{ asset("storage/x.png") }}"
                                                           alt="Delete">
                                                    <a class="ml-3"
                                                       href="{{ route("comment.edit",[$comment->id]) }}">Edit</a>
                                                </form>
                                            @endif

                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
