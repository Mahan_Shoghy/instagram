@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="w-100 justify-content-center">{{ $posts->links() }}</div>
            <div class="col-md-8">
                @foreach($posts as $post)
                    <div class="card mb-5">
                        <div class="card-header">
                            <div>
                                <a href="{{ route("user.show", [$post->users->username]) }}">
                                    <img style="border-radius: 100%; border: black 0.3px solid" src="
                                    @if($post->users->img_profile == null)
                                    {{ asset("storage/profile/default.png") }}
                                    @else
                                    {{ asset("storage/profile/".$post->users->img_profile) }}
                                    @endif " alt="add post" width="35px" height="35px">
                                    ---- {{ $post->users->username }}

                                </a>
                                <div style="display: inline-block; float: right"
                                     class="card-subtitle text-muted">{{ $post->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-3">
                            <div id="carouselExampleInterval{{ $post->id }}" class="carousel slide"
                                 data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php
                                    $num = 1;
                                    ?>
                                    @foreach($post->media as $media)
                                        <div class="carousel-item @if($num === 1) active @endif">
                                            @if($media->mime == "img")
                                                <img src="{{ route("download",$media->file) }}"
                                                     height="400px" class="w-100">
                                            @else
                                                <video class="w-100" style="height: 400px;" controls>
                                                    <source src="{{ route("download",$media->file) }}">
                                                    Your browser does not support HTML5 video.
                                                </video>
                                            @endif
                                        </div>
                                        <?php
                                        $num += 1;
                                        ?>
                                    @endforeach
                                </div>
                                @if($post->media->count() > 1)
                                    <a class="carousel-control-prev"
                                       href="#carouselExampleInterval{{ $post->id }}"
                                       role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next"
                                       href="#carouselExampleInterval{{ $post->id }}"
                                       role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                @endif
                            </div>
                            <div class="row mt-2" style="margin-left: 0.1%;">
                                <div class="col-md-1">
                                    @if(!$post->likeable->contains('id', Auth()->id()))
                                        <a href="{{ route("post.like",[$post->id]) }}"><img
                                                style="width: 30px;" src="{{ asset("storage/unlike.png") }}"></a>
                                        <a href="{{ route("post.like.list",[$post->id]) }}" style="margin-left: 9.5px;">{{ $post->count_like }}</a>
                                    @else
                                        <a href="{{ route("post.dislike",[$post->id]) }}"><img
                                                style="width: 30px;" src="{{ asset("storage/like.png") }}"></a>
                                        <a href="{{ route("post.like.list",[$post->id]) }}" style="margin-left: 9.5px;">{{ $post->count_like }}</a>
                                    @endif
                                </div>
                                <div class="col-md-1">
                                    <a href="{{ route("post.show", [$post->id]) }}">
                                        <img src="{{ asset("storage/comment.png") }}">
                                        <div style="margin-left: 9.5px;">{{ $post->comment->count() }}</div>
                                    </a>
                                </div>
                            </div>
                            <div class="card-body">
                                <p class="card-text">{{ $post->caption }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    </div>
@endsection
